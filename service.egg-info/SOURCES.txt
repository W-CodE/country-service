MANIFEST.in
README.md
license.txt
requirements.txt
setup.py
service/__init__.py
service/hooks.py
service/modules.txt
service/patches.txt
service.egg-info/PKG-INFO
service.egg-info/SOURCES.txt
service.egg-info/dependency_links.txt
service.egg-info/not-zip-safe
service.egg-info/top_level.txt
service/config/__init__.py
service/config/desktop.py
service/config/docs.py
service/service/__init__.py
service/templates/__init__.py
service/templates/pages/__init__.py