# Copyright (c) 2022, Nakul P Kumar and contributors
# For license information, please see license.txt
import frappe
from frappe import _
from frappe.model.document import Document
from frappe.core.doctype.communication.email import make


class CountryServices(Document):
	def before_save(self):
			self.name1 = f'{self.country} {self.service}'
			self.notification()

	def notification(self):
		if self.due_date:
			template = "notification"
			args = {
				"service": self.service,
				"country": self.country,
				"nameid": self.nameid,
				"due_date": self.due_date,
				"message_to_be_flashed_with_reminder":self.message_to_be_flashed_with_reminder
			}
			frappe.sendmail(
				recipients = ['nakulwild@gmail.com'],
				template = template,
				args = args,
				subject = _("Country Service Created"),

			)
