// Copyright (c) 2022, Nakul P Kumar and contributors
// For license information, please see license.txt

frappe.ui.form.on('Country Services', {
	frequency: function(frm, cdt, cdn) {
		set_datas(frm, cdt, cdn);
	},
	no: function(frm, cdt, cdn) {
		var reminder = locals[cdt][cdn]
		var drm = frm.doc.dorm
		var no = frm.doc.no
		var nft = frm.doc.next_frequent_date
		if(drm == "Month"){
			var days = no * 30
			const d = new Date(nft);
			d.setDate(d.getDate() - days);
			var remind_date = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate() ;
			console.log(remind_date)
			}
		else{
			var days = no
			const d = new Date(nft);
			d.setDate(d.getDate() - days);
			var remind_date = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate() ;
			console.log(remind_date)
		}
		frappe.model.set_value(reminder.doctype, reminder.name, "remind_date",remind_date);
	},
	

});
let set_datas = function(frm, cdt, cdn) {
	var d = frappe.model.get_doc(cdt, cdn);
	var next_frequent_date = d.next_frequent_date;
	var frequency = d.frequency;
	var remind_date = d.remind_date

	if (frequency == "Annual"){
		const d = new Date();
		d.setDate(d.getDate() + 365);
		var nxt = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
		// var nxt = d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear();
		
	}
	else if(frequency == "Quarterly"){
		const d = new Date();
		d.setDate(d.getDate() + 91);
		var nxt = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
		

	}
	else if(frequency == "Monthly"){
		const d = new Date();
		d.setDate(d.getDate() + 30);
		var nxt = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
		

	}
	else if(frequency == "Half yearly"){
		const d = new Date();
		d.setDate(d.getDate() + 181);
		var nxt = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
		

	}
	else if(frequency == "Weekly"){
		const d = new Date();
		d.setDate(d.getDate() + 7);
		var nxt = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
		

	}
	else if(frequency == "Custom"){
		let b = new frappe.ui.Dialog({
			title: 'Enter details',
			fields: [
				{
					label: 'Date or Month',
					fieldname: 'dorm',
					fieldtype: 'Select',
					options: "Date\nMonth",
		
				},
				{
					label: 'Number of (Days/Months)',
					fieldname: 'number',
					fieldtype: 'Int'
				}
			],
			primary_action_label: 'Set',
			primary_action(values) {
				if(values.dorm == "Month"){
					const d = new Date();
					var day = 30*values.number
					d.setDate(d.getDate() + day);
					var nxt = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
				}
				else{
					const d = new Date();
					var day = values.number
					d.setDate(d.getDate() + day);
					var nxt = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
					
				}
				frappe.model.set_value(d.doctype, d.name,"next_frequent_date",nxt);	
				b.hide();
			}
			
		});
		b.show();
	}
	
	
	frappe.model.set_value(d.doctype, d.name, "next_frequent_date",nxt);
		
}

